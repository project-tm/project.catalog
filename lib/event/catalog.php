<?

namespace Project\Catalog\Event;

use CIBlockElement,
    CPrice,
    CCatalogSKU,
    CCatalogProduct,
    CCurrencyRates,
    Project\Catalog\Config;

class Catalog {
    /*
      "ELEMENT_SORT_FIELD" => "PROPERTY_SYSTEM_PRICE",
      "ELEMENT_SORT_ORDER" => "DESC",
     */

    /*
      Скрыть отсутствующие товары:

      "HIDE_NOT_AVAILABLE" => "N",
      Отсортировать по популярности, но показать сначала товары в наличии, затем отсутствующие:

      "ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
      "ELEMENT_SORT_ORDER" => "DESC",
      "ELEMENT_SORT_FIELD2" => "SHOWS",
      "ELEMENT_SORT_ORDER2" => "DESC",
      Отсортировать по наличию:

      "ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
      "ELEMENT_SORT_ORDER" => "DESC",
     */

    public static function update($ID, $isQuality = true, $isPrice = true) {
        $arProductInfo = CCatalogSKU::GetProductInfo($ID);
        if (is_array($arProductInfo)) {
            $arFilter = array(
                'IBLOCK_ID' => $arProductInfo['OFFER_IBLOCK_ID'],
                "PROPERTY_CML2_LINK" => $arProductInfo['ID'],
            );

            $quantity = $priceMin = $priceMax = 0;
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("CATALOG_QUANTITY", 'CATALOG_GROUP_' . Config::PRICE_ID));
            while ($arOffers = $res->Fetch()) {
//                $arOffers['CATALOG_CURRENCY_' . Config::PRICE_ID] = 'USD';
                if ($arOffers['CATALOG_CURRENCY_' . Config::PRICE_ID] != Config::CURRENCE) {
                    $price = CCurrencyRates::ConvertCurrency($arOffers['CATALOG_PRICE_' . Config::PRICE_ID], $arOffers['CATALOG_CURRENCY_' . Config::PRICE_ID], Config::CURRENCE);
                } else {
                    $price = $arOffers['CATALOG_PRICE_' . Config::PRICE_ID];
                }
                if ($price > $priceMax) {
                    $priceMax = $price;
                }
                if (empty($priceMin) or $priceMin > $price) {
                    $priceMin = $price;
                }
//                if ($quantity < $arOffers["CATALOG_QUANTITY"]) {
//                    $quantity = $arOffers["CATALOG_QUANTITY"];
//                }
                $quantity += $arOffers["CATALOG_QUANTITY"];
            }
            $arFields = array(
                "QUANTITY" => $quantity
            );
            $propFields = array(
                'MINIMUM_PRICE' => $priceMin,
                'MAXIMUM_PRICE' => $priceMax,
            );

            if ($isQuality) {
                $arCatalog = CCatalogProduct::GetByID($arProductInfo['ID']);
                if ($arCatalog) {
                    CCatalogProduct::Update($arProductInfo['ID'], $arFields);
                } else {
                    $arFields['ID'] = $arProductInfo['ID'];
                    CCatalogProduct::Add($arFields);
                }
            }

            if ($isPrice) {
                foreach ($propFields as $key => $value) {
                    CIBlockElement::SetPropertyValues($arProductInfo['ID'], $arProductInfo['IBLOCK_ID'], $value, $key);
                    pre($arProductInfo['ID'], $arProductInfo['IBLOCK_ID'], $value, $key);
                }
            }
        }
        return $arProductInfo;
    }

    public static function OnPriceUpdate($ID, $arFields) {
        $GLOBALS['APPLICATION']->RestartBuffer();

        if (empty($arFields["PRODUCT_ID"])) {
            $arPrice = CPrice::GetByID($ID);
            $arProductInfo = self::update($arPrice["PRODUCT_ID"], false, true);
        } else {
            $arProductInfo = self::update($arFields["PRODUCT_ID"], false, true);
        }

        if (empty($arProductInfo)) {
            if (empty($arFields["PRODUCT_ID"])) {
                $arFields = CPrice::GetByID($ID);
            }
            if ($arFields['CURRENCY'] != Config::CURRENCE) {
                $price = CCurrencyRates::ConvertCurrency($arFields['PRICE'], $arFields['CURRENCY'], Config::CURRENCE);
            } else {
                $price = $arFields['PRICE'];
            }
            $propFields = array(
                'MINIMUM_PRICE' => $price,
                'MAXIMUM_PRICE' => $price,
            );
            foreach ($propFields as $key => $value) {
                CIBlockElement::SetPropertyValueCode($arFields["PRODUCT_ID"], $key, $value);
            }
        }
//        preExit($ID);
    }

    public static function OnProductUpdate($ID, $arFields) {
//        preDebug(__FUNCTION__);
        self::update($ID, true, false);
    }

}
